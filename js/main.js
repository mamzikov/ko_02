requirejs.config({
    baseUrl: 'js',
    paths: {
        'jquery': 'vendor/jquery-3.3.1.min',
        'bootstrap': 'vendor/bootstrap/js/bootstrap.bundle.min',
        'maskedinput': 'vendor/jquery.inputmask.bundle',
        'TimelineMax': 'vendor/TimelineMax.min',
        'TweenMax': 'vendor/TweenMax.min',
        'TweenLite': 'vendor/TweenLite.min',
        'fullpage': 'vendor/jquery.fullpage.min',
        'validator': 'vendor/validator.min',
        'countTo': 'vendor/jquery.countTo',
    },
    waitSeconds: 0
});

require([
    'jquery',
    'maskedinput',
    'TweenMax',
    'TweenLite',
    'fullpage',
    'validator',
    'TimelineMax',
    'countTo',
    'bootstrap',

], function($, inputmask, TweenMax, TweenLite, fullpage, validator, countTo) {

    //валидация форм
    //Проверка поля имя
    function checkName(elem) {
        var result = true;
         
        if(!validator.isAlpha(elem.val(), ['ru-RU'])) {
            result = false;
        }

        if(!validator.isLength(elem.val(), {min: 3, max: 20})) {
            result = false;
        }

        return result;
    }

    //Проверка поля телефон
    function checkPhone(elem) {
        return validator.isMobilePhone(elem.val(), ['ru-RU']);
    }

    //Проверка поля email
    function checkEmail(elem) {
        return validator.isEmail(elem.val());
    }

    //Проверка поля регион
    function checkLocation(elem) {
        return elem.val() != 0;
    }

    //Проверка поля время
    function checkTime(elem) {
        return elem.val() != 0;
    }

    //Функция смены акстивного класса кнопки формы
    function toggleActiveClassButton(fields, form, button) {
        var validFields = true;
        
        for (var i = 0; i < fields.length; i++) {            
            if (!$(form).hasClass('valid' + fields[i])) {
                validFields = false;
            }            
        }

        if (validFields) {           
            button.removeClass('Button--no-active');
            button.prop('disabled', false);
        } else {
            button.addClass('Button--no-active');
            button.prop('disabled', true);
        }
    }

    //Вызов необходимой функции валидации
    function getValidationFunc(fieldName, field) {

        switch (fieldName) {
            case 'Name':
                return checkName(field);
                break;
            case 'Phone':
                return checkPhone(field);
                break;
            case 'Email':
                return checkEmail(field);
                break;
            case 'Location':
                return checkLocation(field);
                break;
            case 'Time':
                return checkTime(field);
                break;
            default:
                console.log('Функции для поля ' + field + ' не существует');  
        }

    }

    //Валидация при вводе символов в поле
    function validationForms(fields, form) {

        fields.forEach(function(field) {
            var selectors = $(form).find('.Form__input-' + field.toLowerCase());
            var validClass = 'valid' + field;

            selectors.each(function(index, element) {
                var elem = $(element);
                var button  = $(form).find('.Button');       
        
                elem.on('input', function(e) {
                    if (elem.val().length === 0) {
                        elem.addClass('empty');
                        elem.removeClass('noValid');
                        elem.removeClass('isValid');
                    } else {
                        elem.removeClass('empty');
        
                        // добавляем полю класс-признак успешной валидации
                        if (!getValidationFunc(field, elem)) {
                            elem.addClass('noValid');
                            elem.removeClass('isValid');
                        } else {
                            elem.removeClass('noValid');
                            elem.addClass('isValid');
                        }                
        
                        // добавляем родительскоей форме класс-признак успешной валидации поля Имя
                        if (elem.hasClass('isValid')) {
                            $(form).addClass(validClass);
                        } else {
                            $(form).removeClass(validClass);
                        }
                        
                        // добавляем кнопке активный класс
                        toggleActiveClassButton(fields, form, button);
                    }
                });
            });
        });
    }

    //запуск валидации на формах блоков Present
    validationForms(['Name', 'Phone'], '.Form--present');

    //запуск валидации на форме в модальном окне
    validationForms(['Name', 'Phone', 'Email', 'Location'], '.Form--modal');

    //запуск валидации на форме кнопки заказа обратного звонка
    validationForms(['Name', 'Phone', 'Location'], '.Form--callback');

    // Проверка поля на заполненость для анимации лейбла
    $('.js-input-animation').on('input propertychange', function(e) {
        var inputText = $(this).val();

        if (inputText) {
            $(this).addClass('has-value');
        } else {
            $(this).removeClass('has-value');
        }
    });

    // Маска для полей под телефонный номер
    $('.Form__input-phone').inputmask({
        mask: '+79999999999',
        showMaskOnHover: false
    });

    //Отправка форм 
    var buttons = $('.js-ajax-send-form');

    buttons.each(function(index, element) {
        var form = $(this).closest('.Form');
        var name = form.find('.Form__input-name');
        var phone = form.find('.Form__input-phone');
        var email = form.find('.Form__input-email');
        var locationValue = form.find('.Form__input-location');
        var timeCallback = form.find('.Form__input-time');
        var valueCount = form.find('.timer:first');
        var button = $(this);

        

        button.on('click', function() {
            var checkedTime = timeCallback.filter(':checked');
            var proceed = true;
            var output = '';
            

            if (proceed) {
                var post_data = {
                'name'     		: name.val(), 
                'phone'    		: phone.val(),
                'email'         : email.val(),
                'locationValue' : locationValue.val(),
                //Заполнение скрытого поля значения счётчика
                'present'       : valueCount.text(),
                'time'          : checkedTime.val(), 
                'domain'		: $(location).attr('href')
                };
            }
            
            //Ajax отправка данных на сервер
            $.post('form/contact.php', post_data, function(response){  
                if (response.type == 'error') { 
                    //вывод сообщений при загрузке     
                    output = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+response.text+'</div>';
                } else {
                    output = '<div class="alert alert-success alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+response.text+'</div>';

                    //сброс данных полей
                    name.val('');
                    name.removeClass('isValid');
                    name.removeClass('has-value');
                    phone.val('');
                    phone.removeClass('isValid');
                    phone.removeClass('has-value');
                    email.val('');
                    email.removeClass('isValid');
                    email.removeClass('has-value');
                    locationValue.val('0');
                    locationValue.removeClass('isValid');
                    button.addClass('Button--no-active');
                    button.attr('disabled', 'disabled');
                    form.find('.cfs_response').slideUp();
                }
                    
                form.find('.cfs_response').hide().html(output).slideDown();

            }, 'json');
        });
    });

    //Preloader
    var preLodr = $('#preloader');
    if (preLodr) {
        $('#preloader').fadeOut();
        $('.loading').delay(350).fadeOut('slow');
        $('body').delay(350).css({ 'overflow': 'visible' });
    }

    // Модуль анимирования объектов страницы
    var animationBlock = (function() {

        function animationFadeInUp(elem) {
            var item = elem.find('.js-animation-fadeInUp');

            // new TimelineMax().to(item, 0.8, { 'transform': 'translate3d(0,0,0)', opacity: 1 }, 0.15);
            TweenLite.to(item, 0.6, { y: -30, opacity: 1, ease: Power2.easeOut });

        }

        function reverseAnimationFadeInUp(elem) {
            var item = elem.find('.js-animation-fadeInUp');

            // new TimelineMax().to(item, 0.3, { 'transform': 'translate3d(0,40px,0)', opacity: 0 }, 0);
            TweenLite.to(item, 0.6, { y: 30, opacity: 0 });
        }

        function animationFadeInDown(elem) {
            var item = elem.find('.js-animation-fadeInDown');

            TweenLite.to(item, 0.6, { y: 30, opacity: 1, ease: Power2.easeOut });

        }

        function reverseAnimationFadeInDown(elem) {
            var item = elem.find('.js-animation-fadeInDown');

            TweenLite.to(item, 0.6, { y: -30, opacity: 0 });
        }

        function animationFadeInUpMulti(elem, step) {
            var items = elem.find('.js-animation-fadeInUp');
            var delayAnimation = step;

            for (var i = 0; i < items.length; i++) {
                TweenLite.to(items[i], 0.6, { y: -30, opacity: 1, ease: Power2.easeOut, delay: delayAnimation });
                delayAnimation = delayAnimation + step;
            }
        }

        function reverseAnimationFadeInUpMulti(elem) {
            var items = elem.find('.js-animation-fadeInUp');

            for (var i = 0; i < items.length; i++) {
                TweenLite.to(items[i], 0.6, { y: 30, opacity: 0 });
            }
        }

        function animationFadeIn(elem) {
            var item = elem.find('.js-animation-fadeIn');

            // new TimelineMax().to(item, 0.8, { opacity: 1 }, 0.15);
            TweenLite.to(item, 0.6, { opacity: 1 });
        }

        function reverseAnimationFadeIn(elem) {
            var item = elem.find('.js-animation-fadeIn');

            TweenLite.to(item, 0.6, { opacity: 0 });
        }

        function animationFadeInMulti(elem, step) {
            var items = elem.find('.js-animation-fadeIn');
            var delayAnimation = step;

            for (var i = 0; i < items.length; i++) {
                TweenLite.to(items[i], 0.6, { opacity: 1, delay: delayAnimation });
                delayAnimation = delayAnimation + step;
            }
        }

        function reverseAnimationFadeInMulti(elem) {
            var items = elem.find('.js-animation-fadeIn');

            for (var i = 0; i < items.length; i++) {
                TweenLite.to(items[i], 0.6, { opacity: 0 });
            }
        }

        function animationSlideLeft(elem) {
            TweenLite.to(elem, 0.6, { left: 0 });
        }

        function reverseAnimationSlideLeft(elem) {
            TweenLite.to(elem, 0.6, { left: -2000 });
        }

        function animationHeightUp(elem, widthElem, heightElem) {
            TweenLite.fromTo(elem, 0.6, { width: 0, height: 0 }, { width: widthElem, height: heightElem });
        }

        function reverseAnimationHeightUp(elem) {
            TweenLite.to(elem, 0.6, { width: 0, height: 0 });
        }


        return {
            animationFadeInUpMulti: animationFadeInUpMulti,
            reverseAnimationFadeInUpMulti: reverseAnimationFadeInUpMulti,
            animationFadeInUp: animationFadeInUp,
            reverseAnimationFadeInUp: reverseAnimationFadeInUp,
            animationFadeInDown: animationFadeInDown,
            reverseAnimationFadeInDown: reverseAnimationFadeInDown,
            animationFadeIn: animationFadeIn,
            reverseAnimationFadeIn: reverseAnimationFadeIn,
            animationFadeInMulti: animationFadeInMulti,
            reverseAnimationFadeInMulti: reverseAnimationFadeInMulti,
            animationSlideLeft: animationSlideLeft,
            reverseAnimationSlideLeft: reverseAnimationSlideLeft,
            animationHeightUp: animationHeightUp,
            reverseAnimationHeightUp: reverseAnimationHeightUp
        };
    }());

    //Функция счетчика
    function count(selector, delay) {
        if ($(selector).text() > 0) {
            setTimeout(function(){
                $(selector).countTo();
              }, delay);
        }        
    }

    //Функция определения позиции блока при скролле
    function positionBlock(block, selector) {
        var windowBottom = $(window).scrollTop() + $(window).height();
        var block2Bottom = block.offset().top + $(selector).height();
        return windowBottom >= block2Bottom;
    }

    //Запускаем счетчик суммы подарка на мобильных устройствах
    if ($(window).width() < 768) {
        var block = $('.Present--first');

        $(window).scroll(function() {
            if (positionBlock(block, '.Present__value--first')) {
                count('.timer', 3000);
            }
        });        
    }

    //Открытие формы обратный звонок
    $('.js-callback-open').on('click', function(e) {
        e.preventDefault();

        var formBlock = $(this).closest('.Callback');
        var textButton = $(this).find('.Callback__text');

        formBlock.toggleClass('is-active');
        
        if (formBlock.hasClass('is-active')) {
            textButton.text('свернуть');
        } else {
            if ($(window).width() >= 768) {
                textButton.text('Хотите мы вам перезвоним?');
            } else {
                textButton.html('<i class="icon icon-phone"></i>');
            }
        }
    });

    // Смена текста кнопки на иконку на мобильных
    function changeTextCallback() {
        if ($(window).width() < 768) {
            $('.Callback__text').html('<i class="icon icon-phone"></i>');
        }
    }
    
    changeTextCallback();
    

    //Mouse
    var mouse = $('.Mouse');

    // Поэкранная прокрутка
    if ($(window).width() >= 768) {
        $('.js-fullpage-wrapper').fullpage({
            navigation: true,
            navigationPosition: 'right',
            lazyLoading: true,
            fadingEffect: true,
            scrollingSpeed: 850,


            //события после загрузки блоков
            afterLoad: function(anchorLink, index) {
                if (index == 1) {
                    animationBlock.animationFadeInUpMulti($('.Banner__content.js-animation-section-items'), 0.1);
                    mouse.removeClass('Mouse--black');
                }

                if (index == 2) {
                    animationBlock.animationFadeIn($('.Features__header-wrapper.js-animation-section-item'));
                    animationBlock.animationFadeInUpMulti($('.Features__items.js-animation-section-items'), 0.1);
                }

                if (index == 3) {
                    animationBlock.animationSlideLeft($('.js-animation-slide-left'));
                    animationBlock.animationFadeInUp($('.Description__wrapper.js-animation-section-item'));
                    animationBlock.animationFadeInDown($('.Description__wrapper.js-animation-section-item'));

                    mouse.removeClass('Mouse--black');
                }

                if (index == 4) {
                    animationBlock.animationFadeIn($('.Consumers__header-wrapper.js-animation-section-item'));
                    animationBlock.animationFadeInUpMulti($('.Consumers__items.js-animation-section-items'), 0.1);

                    mouse.addClass('Mouse--black');
                }

                if (index == 5) {
                    animationBlock.animationFadeInUpMulti($('.Clients__wrapper.js-animation-section-items'), 0.1);

                    mouse.addClass('Mouse--black');
                }

                if (index == 6) {
                    animationBlock.animationFadeInDown($('.Work__item.js-animation-section-item'));
                    animationBlock.animationFadeIn($('.Work__header-wrapper.js-animation-section-item'));
                    animationBlock.animationFadeInUpMulti($('.Work__items.js-animation-section-items'), 0.1);
                }

                if (index == 7) {
                    animationBlock.animationFadeIn($('.Provide__header-wrapper.js-animation-section-item'));
                    animationBlock.animationFadeInUpMulti($('.Provide__items.js-animation-section-items'), 0.1);
                }

                if (index == 8) {
                    animationBlock.animationFadeInDown($('.Support__item.js-animation-section-item'));
                    animationBlock.animationFadeIn($('.Support__header-wrapper.js-animation-section-item'));
                    animationBlock.animationFadeInUpMulti($('.Support__items.js-animation-section-items'), 0.1);

                    mouse.removeClass('Mouse--black');

                }

                if (index == 9) {
                    animationBlock.animationFadeIn($('.Money__header-wrapper.js-animation-section-item'));
                    animationBlock.animationFadeInUpMulti($('.Money__items.js-animation-section-items'), 0.1);

                    mouse.addClass('Mouse--black');
                }

                if (index == 10) {
                    animationBlock.animationFadeIn($('.Partners__header-wrapper.js-animation-section-item'));
                    animationBlock.animationFadeIn($('.Partners__button-wrapper.js-animation-section-item'));
                    animationBlock.animationFadeInMulti($('.Partners__items.js-animation-section-items'), 0.1);
                    animationBlock.animationHeightUp($('.Partners__photo--small.js-animation-heightUp'), 200, 200);
                    animationBlock.animationHeightUp($('.Partners__photo--large.js-animation-heightUp'), 250, 250);

                    mouse.addClass('Mouse--black');
                }

                if (index == 11) {
                    animationBlock.animationFadeInMulti($('.Present--black.js-animation-section-items'), 0.1);
                    mouse.removeClass('Mouse--black');

                    //Счетчик суммы подарка
                    if ($('.timer').text() > 0) {
                        count('.timer', 3000);
                    }                    
                }

                if (index == 12) {
                    animationBlock.animationFadeInMulti($('.Present--white.js-animation-section-items'), 0.1);                    
                }
            },
            onLeave: function(index, nextIndex, direction) {

                if (index == 1) {
                    animationBlock.reverseAnimationFadeInUpMulti($('.Banner__content.js-animation-section-items'));
                    mouse.addClass('Mouse--black');
                }

                if (index == 2) {
                    animationBlock.reverseAnimationFadeIn($('.Features__header-wrapper.js-animation-section-item'));
                    animationBlock.reverseAnimationFadeInUpMulti($('.Features__items.js-animation-section-items'));
                    mouse.removeClass('Mouse--black');
                }

                if (index == 3) {
                    var itemWrapper = $('.Description__wrapper.js-animation-section-item');

                    animationBlock.reverseAnimationSlideLeft($('.js-animation-slide-left'));
                    animationBlock.reverseAnimationFadeInUp(itemWrapper);
                    animationBlock.reverseAnimationFadeInDown(itemWrapper);
                    mouse.addClass('Mouse--black');
                }

                if (index == 4) {
                    animationBlock.reverseAnimationFadeIn($('.Consumers__header-wrapper.js-animation-section-item'));
                    animationBlock.reverseAnimationFadeInUpMulti($('.Consumers__items.js-animation-section-items'));
                }

                if (index == 5) {
                    animationBlock.reverseAnimationFadeInUpMulti($('.Clients__wrapper.js-animation-section-items'));
                    mouse.removeClass('Mouse--black');
                }

                if (index == 6) {
                    animationBlock.reverseAnimationFadeInDown($('.Work__item.js-animation-section-item'));
                    animationBlock.reverseAnimationFadeIn($('.Work__header-wrapper.js-animation-section-item'));
                    animationBlock.reverseAnimationFadeInUpMulti($('.Work__items.js-animation-section-items'));
                }

                if (index == 7) {
                    animationBlock.reverseAnimationFadeIn($('.Provide__header-wrapper.js-animation-section-item'));
                    animationBlock.reverseAnimationFadeInUpMulti($('.Provide__items.js-animation-section-items'));
                }

                if (index == 8) {
                    animationBlock.reverseAnimationFadeInDown($('.Support__item.js-animation-section-item'));
                    animationBlock.reverseAnimationFadeIn($('.Support__header-wrapper.js-animation-section-item'));
                    animationBlock.reverseAnimationFadeInUpMulti($('.Support__items.js-animation-section-items'));
                }

                if (index == 9) {
                    animationBlock.reverseAnimationFadeIn($('.Money__header-wrapper.js-animation-section-item'));
                    animationBlock.reverseAnimationFadeInUpMulti($('.Money__items.js-animation-section-items'));
                }

                if (index == 10) {
                    animationBlock.reverseAnimationFadeIn($('.Partners__header-wrapper.js-animation-section-item'));
                    animationBlock.reverseAnimationFadeIn($('.Partners__button-wrapper.js-animation-section-item'));
                    animationBlock.reverseAnimationFadeInMulti($('.Partners__items.js-animation-section-items'));
                    animationBlock.reverseAnimationHeightUp($('.Partners__photo.js-animation-heightUp'));
                    mouse.removeClass('Mouse--black');
                }

                if (index == 11) {
                    animationBlock.reverseAnimationFadeInMulti($('.Present--black.js-animation-section-items'));
                    mouse.addClass('Mouse--black');
                }

                if (index == 12) {
                    animationBlock.reverseAnimationFadeInMulti($('.Present--white.js-animation-section-items'));
                    mouse.addClass('Mouse--black');
                }
            }
        });
    }    
});